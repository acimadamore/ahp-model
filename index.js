AHPLevel  = require('./dist/model/AHPLevel.js').default
Criterion = require('./dist/model/Criterion.js').default


let l = new AHPLevel([
  new Criterion('Price'),
  new Criterion('Technical Specs.', [
    new Criterion('Processor'),
    new Criterion('Memory', [
      new Criterion('RAM'),
      new Criterion('Storage')
    ])
  ]),
  new Criterion('Reviews', [
    new Criterion('Amazon Stars'),
    new Criterion('GSMArena Points')
  ])
])