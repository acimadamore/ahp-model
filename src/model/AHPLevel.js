import AHPRanking         from './AHPRanking'
import Criterion          from './Criterion'
import DecisionMatrix     from './DecisionMatrix'
import PairwiseComparison from './PairwiseComparison'

export default class {

  constructor(criteria = []) {
    this.criteria            = criteria
    this.pairwiseComparisons = []
  }

  hasCriteria() {
    return this.criteria.length > 0
  }

  getCriterionByName(name) {
    let idx = this.criteria.findIndex(c => c.name == name)

    return (idx != -1) ? this.criteria[idx] : null
  }

  addCriterion(criterion) {
    let c = this.getCriterionByName(criterion.name)

    if(c == null){
      this.criteria.push(criterion)
    }
  }

  removeCriterion(criterion) {
    let c = this.getCriterionByName(criterion.name)

    if (c != null) {
      // TODO refactor this
      let idx = this.pairwiseComparisons.findIndex(pwc => pwc.criteria1 == c || pwc.criteria2 == c)

      while(idx != -1){
        this.pairwiseComparisons.splice(idx, 1)

        idx = this.pairwiseComparisons.findIndex(pwc => pwc.criteria1 == c || pwc.criteria2 == c)
      }

      this.criteria.splice(this.criteria.indexOf(c), 1)
    }
  }

  compare(criterion1, criterion2, value) {
    let c1 = this.getCriterionByName(criterion1.name)
    let c2 = this.getCriterionByName(criterion2.name)

    if(c1 != null && c2 != null){
      let pwc = new PairwiseComparison(c1, c2, value)

      if(pwc.isValid()){
        // TODO refactor this
        let idx = this.pairwiseComparisons.findIndex(pwc => (pwc.criteria1 == c1 && pwc.criteria2 == c2) || (pwc.criteria1 == c2 && pwc.criteria2 == c1))

        while(idx != -1){
          this.pairwiseComparisons.splice(idx, 1)

          idx = this.pairwiseComparisons.findIndex(pwc => (pwc.criteria1 == c1 && pwc.criteria2 == c2) || (pwc.criteria1 == c2 && pwc.criteria2 == c1))
        }

        this.pairwiseComparisons.push(pwc)
      }
    }
  }

  rank() {
    let dm = this.buildDecisionMatrix()

    if(dm.isValid()){
      return this.createRanking(dm.calculatePriorities())
    }
  }

  createRanking(rankingValues) {
    let ranking = new AHPRanking()

    for(let i=0; i < rankingValues.length; i++)
      ranking.add(this.criteria[i], rankingValues[i])

    return ranking
  }


  buildDecisionMatrix() {
    let dm = new DecisionMatrix(this.criteria.length)

    for(let pwc of this.pairwiseComparisons){
      let c1idx = this.criteria.indexOf(pwc.criterion1)
      let c2idx = this.criteria.indexOf(pwc.criterion2)

      dm.setCell(c1idx, c2idx, pwc.value)
      dm.setCell(c2idx, c1idx, pwc.inverse())
    }

    return dm
  }
}
