import AHPRankedItem from './AHPRankedItem'

export default class {

  constructor() {
    this.ranking = []
  }

  add(item, weight) {
    this.ranking.push(new AHPRankedItem(item, weight));

    this.sortRankingByWeight();
  }

  sortRankingByWeight() {
    this.ranking.sort((i1, i2) => i2.weight - i1.weight)
  }
}
