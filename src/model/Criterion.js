import AHPLevel from './AHPLevel'

export default class {

  constructor(name, subcriteria = []) {
    this.name            = name
    this.subcriteria     = new AHPLevel(subcriteria)
    this.rankingStrategy;
  }

  toString() {
    return this.name
  }

  // Should return AHPRanking
  rankAlternatives(alternatives) {
    this.rankingStrategy.rank(alternatives)
  }
}
