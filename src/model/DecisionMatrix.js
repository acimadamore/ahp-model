export default class {

  constructor(size) {
    this.matrix = new Array(size)

    this.reset()
  }

  toString() {
    let str = ''

    for(let row of this.matrix)
      str += row.join(' ') + '\n'

    return str
  }

  setCell(row, col, value) {
    this.matrix[row][col] = value;
  }

  calculatePriorities() {
    let colSums = [], priorities = []

    for(let i = 0; i < this.matrix.length; i++) colSums[i] = priorities[i] = 0

    for(let row = 0; row < this.matrix.length; row++)
      for(let col = 0; col < this.matrix.length; col++)
        colSums[col] += this.matrix[row][col];

    for(let row = 0; row < this.matrix.length; row++)
      for(let col = 0; col < this.matrix.length; col++)
        priorities[row] += this.matrix[row][col] / colSums[col]

    for(var i = 0; i < this.matrix.length; i++) priorities[i] = priorities[i] / this.matrix.length;


    return priorities;
  }

  reset() {
    for(let i = 0; i < this.matrix.length; i++)
      this.matrix[i] = Array(this.matrix.length)

    for(let i = 0; i < this.matrix.length; i++)
      for(let j = 0; j < this.matrix.length; j++)
        this.matrix[i][j] = (i == j) ? 1 : 0
  }

  isValid() {
    for(let row = 0; row < this.matrix.length; row++)
      for(let col = 0; col < this.matrix.length; col++)
        if(!this.validateCell(row, col)) return false

    return true;
  }

  validateCell(row, col) {
    const allowedValues = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 1/2, 1/3, 1/4, 1/5, 1/6, 1/7, 1/8, 1/9 ]

    return ((row == col) && (this.matrix[row][col] == 1))
        || (allowedValues.includes(this.matrix[row][col]) && (this.matrix[row][col] * this.matrix[col][row] == 1))
  }
}
