export default class {

  constructor(criterion1, criterion2, value = 0){
    this.criterion1 = criterion1
    this.criterion2 = criterion2
    this.value      = value
  }

  isValid() {
    return [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 1/2, 1/3, 1/4, 1/5, 1/6, 1/7, 1/8, 1/9 ].includes(this.value)
  }

  inverse() {
    return (this.value != 0) ? 1/this.value : 0
  }
}
