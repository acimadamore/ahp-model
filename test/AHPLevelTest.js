import 'chai/register-should'

import AHPLevel  from '../src/model/AHPLevel'
import Criterion from '../src/model/Criterion'

describe('AHPLevel', function(){

  describe('#addCriterion()', function(){

    it('should add valid criterion', function(){
      let c = new Criterion('TestCriterion');
      let l = new AHPLevel();

      l.criteria.should.be.empty;

      l.addCriterion(c);

      l.criteria.should.have.lengthOf(1);
      l.criteria.should.include(c);
    });

    it('shouldn\'t add criterion with same name as an existent one', function(){
      let c = new Criterion('TestCriterion');
      let l = new AHPLevel([c]);

      l.criteria.should.have.lengthOf(1);
      l.criteria.should.include(c);

      l.addCriterion(c);

      l.criteria.should.have.lengthOf(1);
    });
  });

  describe('#removeCriterion()', function(){

    it('should remove existent criterion', function(){
      let c = new Criterion('TestCriterion');
      let l = new AHPLevel([c]);

      l.criteria.should.have.lengthOf(1);

      l.removeCriterion(c);

      l.criteria.should.be.empty;
    });

    it('should remove involved pairwise comparisons on criterion removal', function(){
      let c1 = new Criterion('TestCriterion1');
      let c2 = new Criterion('TestCriterion2');
      let l  = new AHPLevel([c1, c2]);

      l.compare(c1, c2, 1);

      l.criteria.should.have.lengthOf(2);
      l.pairwiseComparisons.should.have.lengthOf(1);

      l.removeCriterion(c1);

      l.criteria.should.have.lengthOf(1);
      l.pairwiseComparisons.should.be.empty;
    });

  });

  describe('#compare()', function(){

    it('should be possible to compare two different existent criterion with a valid value', function(){

    });

    it('should override existent pairwise comparison if one with same pair of criterion already exists', function(){

    });

    it('should delete a previous pairwise comparison if it has the same criterions in inversed order', function(){

    })

    it('shouldn\'t be possible to compare if criterion doesn\'t exists', function(){

    });

    it('shouldn\'t be possible to compare a criterion with itself', function(){

    });


  });

});
